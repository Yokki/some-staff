
#include <math.h>
#include <iostream>
#include <sstream>
#include <string>
#include <locale.h>
#include <iomanip>
#include <cstring>  //strchr(string, 'sumb') <-- первое вхождение
#include <cstdlib>
using namespace std;

void FillHelix( int ** &Mas, int sizeY, int sizeX ) {
    int tempy = -1; int tempx = 0;
    int i = 1;
    int size = sizeY*sizeX;
    int spiral = 0;
    while (i <= size) {
        while ((i <= size) && (tempy + 1 < sizeX - spiral)) {
            tempy++;
            Mas[tempx][tempy] = i;
            i++;
        }
        while ((i <= size) && (tempx + 1 < sizeY - spiral)) {
            tempx++;
            Mas[tempx][tempy] = i;
            i++;
        }
        while ((i <= size) && (tempy - 1 >= spiral)) {
            tempy--;
            Mas[tempx][tempy] = i;
            i++;
        }
        spiral++;
        while ((i <= size) && (tempx - 1 >= spiral)) {
            tempx--;
            Mas[tempx][tempy] = i;
            i++;
        }
    }
   
    
    
}
int** newarr (int n, int m) {
    int **a = new int*[n];
    for(int i = 0; i < n; i++) a[i] = new int[m];
    return a;
}
void matrix_output(int**a, int n, int m) {
    for (int i = 0; i < n; i++){
        for (int j = 0; j < m; j++)    cout << a[i][j] << " ";
        cout << endl;
    }
}
int** matrix_null (int**a, int n, int m) {
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
            a[i][j] = 0;
    return a;
}


int main() {
    int n, m;
    char input[256], h;
    cin>> input;
    stringstream stream(input);
    
    stream >> n >> h>> m;
    if (stream) {
        int**a=newarr(n,m);
        a=matrix_null(a, n, m);
        FillHelix(a, n, m);
        matrix_output(a, n,m);
    }
    else {cout << "An error has occurred while reading input data"<<endl;}
    
    
    return 0;
    
    
    
    
}

