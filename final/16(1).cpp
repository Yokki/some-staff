#include <iostream>
#include <math.h>

int main () {
    int n;
    scanf("%d", &n);
    int a[n];
    for (int i=0; i< n; i++){
        a[i]=rand()%100;
        printf("%-4d", a[i]);
    }
    int min = a[0] + a[1];
    for(int i = 1; i < n-1; i++){
        if (a[i]+a[i+1] < min) min= a[i]+a[i+1];
    }
    printf("\n%d", min);
    return 0;
}
