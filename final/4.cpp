
#include <iostream>
#include <math.h>

double function(double x1, double x2, double y1, double y2) {
    return sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
}

int main () {
    double x, y;
    scanf("%lf%lf", &x, &y);
    double rast=function(-1, x, 0, y);
    if ((rast <= 1)&&(y>=(x+1)*(x+1))) {
        printf("YES");
    }
    else {
        printf("NO");
    }
    return 0;
}
