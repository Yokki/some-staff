#include <stdio.h>
#include <math.h>

int func(int a[][], int n, int m){
    int sum=0;
    for(int i=0; i< m; i++){
        sum += a[n][i];
    }
    return sum;
}

int main() {
    int n,m;
    scanf("%d%d", &n, &m);
    int a[n][m];
    int sum =0;
    for (int i =0; i<n; i++){
        for (int j = 0; j < m; j++){
            a[i][j] = rand()%100;
        }
    }
    for (int i = 0; i < n; i++){
        sum +=func(a, i, m);
    }
    printf("%d", sum);
    return 0;
}
