#include <stdio.h>
#include <math.h>

int fibb(int i){
    if ((i == 1)||(i==2)) {return 1;}
    else return fibb(i-1)+fibb(i-2);
}
int main (){
    int k;
    scanf("%d", &k);
    int sum =0;
    for (int i = 1; i <= k; i++){
        sum+=fibb(i);
    }
    printf("%d", sum);
    return 0;
}
