#include <stdio.h>
#include <math.h>

int fact(int i) {
    if ((i == 1) || (i==0)) return 1;
    else {
        i *= fact(i-1);
        return i;
    }
}

int main() {
    double sinx = 0;
    int k = 1;
    for (int i = 1;(M_PI/3)/fact(i) >= 0.0001;i+=2, k*=-1){
        sinx += (k*(M_PI/3)/fact(i));
        printf("%.4lf\n", sinx);
    }
    printf("%.4lf", sinx);
    return 0;
}
