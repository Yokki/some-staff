#include <iostream>

int function(int i) {
    if (i == 1 || i == 2) return -99;
    else return function(i-1) + function(i-2) + 120;
    
}

int main(int argc, const char * argv[]) {
    int i;
    for (i = 3;; i++){
        if (function(i) > 0 ) {
            break;
            
        }
    }
    printf("%d", i);
    return 0;
}
