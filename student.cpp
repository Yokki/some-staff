//
//  main.cpp
//  14.04
//
//  Created by Игорь on 04.12.17.
//  Copyright © 2017 Игорь. All rights reserved.
//
#include <iostream>
#include <string>
#include <locale.h>
#include <iomanip>
#include <cstring> //strchr(string, 'sumb') <-- первое вхождение
#include <cstdlib>
#include <fstream>

using namespace std;
struct student {
    std::string first_name;
    std::string last_name;
    std::string age;
};

int main()
{
    setlocale(LC_ALL, "rus");
    ofstream fout("students.txt");
    int col;
    cout << "Сколько?" << endl;
    cin >> col;
    for (int i = 0; i < col; i++) {
        student temp;
        cin >> temp.first_name >> temp.last_name >> temp.age;
        fout << temp.first_name << endl;
        fout << temp.last_name << endl;
        fout << temp.age << endl;
    }
    fout.close();
    student* arr = new student[col];

    ifstream fin("students.txt");
    for (int i = 0; i < col; i++) {

        fin >> arr[i].first_name
            >>

            arr[i].last_name >>

            arr[i].age;
    }
    fin.close();
    for (int i = 0; i < col; i++) {
        cout << arr[i].first_name << " " << arr[i].last_name << " " << arr[i].age << endl;
    }
    return 0;
}
