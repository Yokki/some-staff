
#include <iostream>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

int main(void)
{
    int X;
    char vvod;
    bool u = true;
    
    cin >> X;
    if (X <= 0) {
        X = -X;
        u = false;
    }
    
    int* a = new int[X];
    for (int i = 0; i < X; i++) {
        cin
        >> a[i];
        
        vvod = cin.get();
        
        if ((vvod == '\n') && (i != X - 1))
        {
            cout
            << "An error has occurred while reading input data\n";
            delete[] a;
            exit(0);
        }
        
        if ((vvod == ' ') && (i == X - 1))
        {
            cout
            << "An error has occurred while reading input data\n";
            delete[] a;
            exit(0);
        }
    }
    
    if (u == false)
    {
        cout
        << "An error has occurred while reading input data\n";
        delete[] a;
        exit(0);
    }
    
    
    
    for (int i = 0; i < X / 2; i++)
        swap(a[i], a[X - i - 1]);
    
    for (int i = 0; i < X; i++)
        cout << a[i] << " ";
    
    delete[] a;
    return 0;
}

