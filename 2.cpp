//
//  2.cpp
//  
//
//  Created by Игорь on 20.11.17.
//

#include "2.hpp"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

int main(void)
{
    int Sdvig;
    int X;
    char Stroka;
    bool u = true;
    
    cin >> X;
    if (X <= 0) {
        X = -X;
        u = false;
    }
    
    int* a = new int[X];
    for (int i = 0; i < X; i++) {
        cin
        >> a[i];
        
        Stroka = cin.get();
        
        if ((Stroka == '\n') && (i != X - 1))
        {
            cout
            << "An error has occurred while reading input data\n";
            delete[] a;
            exit(0);
        }
        
        if ((Stroka == ' ') && (i == X - 1))
        {
            cout
            << "An error has occurred while reading input data\n";
            delete[] a;
            exit(0);
        }
    }
    cin >> Sdvig;
    
    int *c = new int[Sdvig];
    int *b = new int[X-Sdvig];
    for (int i = 0; i < Sdvig; i++){
        c[i]=a[X-Sdvig+i];
    }
    for (int i = 0; i+Sdvig < X; i++){
        b[i]=a[i];
    }
    
    for (int i = 0; i < (X-Sdvig) / 2; i++)
        swap(b[i], b[X - i - 1 - Sdvig]);
    

    for (int i = 0; i < Sdvig / 2; i++)
        swap(c[i], c[Sdvig - i - 1]);
    
    for (int i = 0; i < X - Sdvig; i++){
        a[i]=b[i];
    }
    for (int i = 0; i+Sdvig < X; i++){
        a[X - Sdvig+i]=c[i];
    }
    
    
    
    if (u == false)
    {
        cout
        << "An error has occurred while reading input data\n";
        delete[] a;
        exit(0);
    }
    

    for (int i = 0; i < X / 2; i++)
        swap(a[i], a[X - i - 1]);
    
    for (int i = 0; i < X; i++)
        cout << a[i] << " ";
    
    delete[] a;
    delete[] b;
    delete[] c;
    return 0;
}


